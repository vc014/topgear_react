import React, { Component } from 'react';
//import './myindex.css';
import Table1 from './Details'

 
var data = [
  {id: 1, name: 'Vaishnavy', value: 20},
  {id: 2, name: 'Varsha', value: 30},
  {id: 3, name: 'Prerna', value: 30},
  {id: 4, name: 'Yash', value: 23},
  {id: 5, name: 'Pranay', value: 25}
];
 
 
export default class Main extends Component {
  render() {
    return (
      <div className="App">
        <h1 className="Table-header">Student Details</h1>
        <Table1 data={data}/>
      </div>
    );
  }
}

