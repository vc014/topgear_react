import React, { Component } from 'react';
//import './myindex.css';
import Table1 from './Table1'

 
var data = [
  {id: 1, name: 'Vaishnavy', value: 'vyz@wip.com'},
  {id: 2, name: 'Varsha', value: 'xyz@wip.com'},
  {id: 3, name: 'Prerna', value: 'pyz@wip.com'},
  {id: 4, name: 'Yash', value: 'yyz@wip.com'},
  {id: 5, name: 'Krishna', value: 'gyz@wip.com'}
];
 
 
export default class App extends Component {
  render() {
    return (
      <div className="App">
        <p className="Table-header">Basic Table</p>
        <Table1 data={data}/>
      </div>
    );
  }
}